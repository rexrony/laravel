<?php
include('header.php');
?>
<div class="primary-container">
	<div class="slides-container">
		<div class="container">
			<div class="row">
				<div class="slide-con col-xs-12">
					<div class="slides">
						<img src="images/slider-img.png" alt="">
						<div class="slider-text-con">
							<div class="slide-caption">
								<h5 class="sml-hd">NOW WITH WIDER RANGE</h5>
								<h1>ASUS EEEBOOK</h1>
								<p>Elegant Pyramid Design With Delicate Texture<br> And Compact Outlook</p>
								<div class="pricing-box-slider"><span>Starting At</span><br> $14.99	</div>
								<div class="slide-readmore"><a href="#_">Shop Now</a></div>
							</div>
						</div>
					</div>
					<div class="slides">
						<img src="images/slider-img.png" alt="">
						<div class="slider-text-con">
							<div class="slide-caption">
								<h5 class="sml-hd">NOW WITH WIDER RANGE</h5>
								<h1>ASUS EEEBOOK</h1>
								<p>Elegant Pyramid Design With Delicate Texture<br> And Compact Outlook</p>
								<div class="pricing-box-slider"><span>Starting At</span><br> $14.99	</div>
								<div class="slide-readmore"><a href="#_">Shop Now</a></div>
							</div>
						</div>
					</div>
					<div class="slides">
						<img src="images/slider-img.png" alt="">
						<div class="slider-text-con">
							<div class="slide-caption">
								<h5 class="sml-hd">NOW WITH WIDER RANGE</h5>
								<h1>ASUS EEEBOOK</h1>
								<p>Elegant Pyramid Design With Delicate Texture<br> And Compact Outlook</p>
								<div class="pricing-box-slider"><span>Starting At</span><br> $14.99	</div>
								<div class="slide-readmore"><a href="#_">Shop Now</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!--.slides-container-->
	<div class="clear"></div>
	<div class="deals_of_day ">
		<div class="container">
			<div class="row">
				<section class="deal-wrap col-xs-12 col-md-12">
					<div class="heading-wrap"><h2>Deal of the days</h2></div>
					<div class="clear"></div>
					<div class="deals-wrapper">
						<div class="dealofday_wrap">
							<div class="product-image col-lg-4">
								<img alt="Simple Product 038" src="images/tablet.png" class="one_image lazy">
							</div><!--.product-image-->
							<div class="product-content col-lg-8">
								<div class="pro-heading"><h3>All-New Fire 7 Tablet with Alexa, 7" Display, 8 GB, Black -  with Special Offers</h3></div>
								<div class="pro-stars-rating"><div id="stars" class="starrr"></div></div>
								<div class="pro-price"><span class="price">$000.00</span></div>
								<div class="pro-text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mollis mattis lectus, sed pharetra arcu efficitur quis. Maecenas semper neque quis magna dignissim, sed ultricies risus gravida. Duis viverra nibh sit amet enim rutrum finibus eget in metus. Fusce dui erat, posuere sit amet turpis non, tempor iaculis eros. Donec malesuada turpis ac enim viverra, vel posuere dolor tempus. Aliquam nec enim sed eros tempus porta. Proin metus eros, mollis ut luctus vel, blandit nec mi. Nulla egestas, felis eu varius mattis, eros augue cursus dui, ac congue lectus nulla eu velit.</p></div>
								<div class="pro-timer">
									<ul>
										<li class="chart" data-percent="26"><span>26</span>Days</li>
										<li class="chart" data-percent="15"><span>15</span>Hours</li>
										<li class="chart" data-percent="50"><span>50</span>Mins</li>
										<li class="chart" data-percent="45"><span>45</span>Secs</li>
									</ul>

								</div>
								<div class="pro-btns">
									<a class="add_to_cart defaultbtn" href="#_">Add to cart</a>
									<a class="compare-btn defaultbtn" href="#_"><i class="zmdi zmdi-refresh-alt"></i></a>
									<a class="favourite-btn defaultbtn" href="#_"><i class="zmdi zmdi-favorite-outline"></i></a>
								</div>
							</div><!--.product-centent-->
						</div>
						<div class="dealofday_wrap">
							<div class="product-image col-lg-4">
								<img alt="Simple Product 038" src="images/tablet.png" class="one_image lazy">
							</div><!--.product-image-->
							<div class="product-content col-lg-8">
								<div class="pro-heading"><h3>All-New Fire 7 Tablet with Alexa, 7" Display, 8 GB, Black -  with Special Offers</h3></div>
								<div class="pro-stars-rating"><div id="stars" class="starrr"></div></div>
								<div class="pro-price"><span class="price">$000.00</span></div>
								<div class="pro-text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mollis mattis lectus, sed pharetra arcu efficitur quis. Maecenas semper neque quis magna dignissim, sed ultricies risus gravida. Duis viverra nibh sit amet enim rutrum finibus eget in metus. Fusce dui erat, posuere sit amet turpis non, tempor iaculis eros. Donec malesuada turpis ac enim viverra, vel posuere dolor tempus. Aliquam nec enim sed eros tempus porta. Proin metus eros, mollis ut luctus vel, blandit nec mi. Nulla egestas, felis eu varius mattis, eros augue cursus dui, ac congue lectus nulla eu velit.</p></div>
								<div class="pro-timer">
									<ul>
										<li class="chart" data-percent="26"><span>26</span>Days</li>
										<li class="chart" data-percent="15"><span>15</span>Hours</li>
										<li class="chart" data-percent="50"><span>50</span>Mins</li>
										<li class="chart" data-percent="45"><span>45</span>Secs</li>
									</ul>

								</div>
								<div class="pro-btns">
									<a class="add_to_cart defaultbtn" href="#_">Add to cart</a>
									<a class="compare-btn defaultbtn" href="#_"><i class="zmdi zmdi-refresh-alt"></i></a>
									<a class="favourite-btn defaultbtn" href="#_"><i class="zmdi zmdi-favorite-outline"></i></a>
								</div>
							</div><!--.product-centent-->
						</div>
						<div class="dealofday_wrap">
							<div class="product-image col-lg-4">
								<img alt="Simple Product 038" src="images/tablet.png" class="one_image lazy">
							</div><!--.product-image-->
							<div class="product-content col-lg-8">
								<div class="pro-heading"><h3>All-New Fire 7 Tablet with Alexa, 7" Display, 8 GB, Black -  with Special Offers</h3></div>
								<div class="pro-stars-rating"><div id="stars" class="starrr"></div></div>
								<div class="pro-price"><span class="price">$000.00</span></div>
								<div class="pro-text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mollis mattis lectus, sed pharetra arcu efficitur quis. Maecenas semper neque quis magna dignissim, sed ultricies risus gravida. Duis viverra nibh sit amet enim rutrum finibus eget in metus. Fusce dui erat, posuere sit amet turpis non, tempor iaculis eros. Donec malesuada turpis ac enim viverra, vel posuere dolor tempus. Aliquam nec enim sed eros tempus porta. Proin metus eros, mollis ut luctus vel, blandit nec mi. Nulla egestas, felis eu varius mattis, eros augue cursus dui, ac congue lectus nulla eu velit.</p></div>
								<div class="pro-timer">
									<ul>
										<li class="chart" data-percent="26"><span>26</span>Days</li>
										<li class="chart" data-percent="15"><span>15</span>Hours</li>
										<li class="chart" data-percent="50"><span>50</span>Mins</li>
										<li class="chart" data-percent="45"><span>45</span>Secs</li>
									</ul>

								</div>
								<div class="pro-btns">
									<a class="add_to_cart defaultbtn" href="#_">Add to cart</a>
									<a class="compare-btn defaultbtn" href="#_"><i class="zmdi zmdi-refresh-alt"></i></a>
									<a class="favourite-btn defaultbtn" href="#_"><i class="zmdi zmdi-favorite-outline"></i></a>
								</div>
							</div><!--.product-centent-->
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="electronic-tabs">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<div id="elect_head" class="heading-wrap"><h2>Electronic</h2></div><!--heading-wrap-->
					<div class="clear"></div>
					<div class="electronic-wrapper">
					<div id="tabs-container">
						<ul class="tabs-menu">
							<li class="current"><a href="#tab-1" class="hh-click1">Bestseller</a></li>
							<li><a href="#tab-2">New Arrivals</a></li>
							<li><a href="#tab-3">NEW PRODUCTS</a></li>
							<li><a href="#tab-4">Hot Deal !</a></li>
						</ul>
						<div class="tab">
							<div id="tab-1" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/home_08.png">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div id="tab-2" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/home_08.png">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div id="tab-3" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/home_08.png">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

							<div id="tab-4" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/home_08.png">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>
					</div><!--.electronic-wrapper--->
					</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="imgs-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-6"><a href="#_"><img src="images/home_14.jpg" alt=""></a></div>
				<div class="col-lg-6"><a href="#_"><img src="images/home_17.jpg" alt=""></a></div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="home-appliance-wrapper">
		<div class="container">
			<div class="row">
			<div class="home-app-wrapper col-lg-12 col-xs-12">
				<div id="home_app_head" class="heading-wrap"><h2>Home Appliance</h2></div><!--heading-wrap-->
				<div class="clear"></div>
				<div id="tabs-container">
						<ul class="tabs-menu">
							<li class="current"><a href="#tab-home_app1" class="hh-click1">Bestseller</a></li>
							<li><a href="#tab-home_app2">New Arrivals</a></li>
							<li><a href="#tab-home_app3">NEW PRODUCTS</a></li>
							<li><a href="#tab-home_app4">Hot Deal !</a></li>
						</ul>
						<div class="tab">
							<div id="tab-home_app1" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/home_app1.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_app2.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/home_app3.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_app2.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="tab-home_app2" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/home_app1.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_app2.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/home_app3.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="tab-home_app3" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/home_08.png">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div id="tab-home_app4" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/home_08.png">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>
					</div><!--.home-app-wrapper--->
			</div><!--.row-->
		</div><!--.container-->
	</div><!--.home-appliance-wrapper-->
	<div class="clear"></div>
	<div class="imgs-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-4"><a href="#_"><img src="images/index_18.jpg" alt=""></a></div>
				<div class="col-lg-4"><a href="#_"><img src="images/index_20.jpg" alt=""></a></div>
				<div class="col-lg-4"><a href="#_"><img src="images/index_22.jpg" alt=""></a></div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="accessories-wrapper">
		<div class="container">
			<div class="row">
			<div class="accessories-app-wrapper col-lg-12 col-xs-12">
				<div id="acc_head" class="heading-wrap"><h2>Accessories</h2></div><!--heading-wrap-->
				<div class="clear"></div>
				<div id="tabs-container">
						<ul class="tabs-menu">
							<li class="current"><a href="#tab-acc_app1" class="hh-click1">Bestseller</a></li>
							<li><a href="#tab-acc_app2">New Arrivals</a></li>
							<li><a href="#tab-acc_app3">NEW PRODUCTS</a></li>
							<li><a href="#tab-acc_app4">Hot Deal !</a></li>
						</ul>
						<div class="tab">
							<div id="tab-acc_app1" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/index_31.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/index_33.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/index_35.png">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_app2.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="tab-acc_app2" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/index_31.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/index_33.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/index_35.png">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_app2.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="tab-acc_app3" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/home_08.png">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div id="tab-acc_app4" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/home_08.png">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>
					</div><!--.home-app-wrapper--->
			</div><!--.row-->
		</div><!--.container-->
	</div><!--.accessories-wrapper-->
	<div class="clear"></div>
	<div class="imgs-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-lg-6"><a href="#_"><img src="images/index_40.jpg" alt=""></a></div>
				<div class="col-lg-6"><a href="#_"><img src="images/index_42.jpg" alt=""></a></div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="fashion-wrapper">
		<div class="container">
			<div class="row">
			<div class="fashions-wrapper col-lg-12 col-xs-12">
				<div id="fash_head" class="heading-wrap"><h2>Fashion</h2></div><!--heading-wrap-->
				<div class="clear"></div>
				<div id="tabs-container">
						<ul class="tabs-menu">
							<li class="current"><a href="#tab-fashion1" class="hh-click1">Bestseller</a></li>
							<li><a href="#tab-fashion2">New Arrivals</a></li>
							<li><a href="#tab-fashion3">NEW PRODUCTS</a></li>
							<li><a href="#tab-fashion4">Hot Deal !</a></li>
						</ul>
						<div class="tab">
							<div id="tab-fashion1" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/index_50.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/index_52.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/index_54.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/index_50.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/index_52.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="tab-fashion2" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/index_31.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/index_33.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/index_35.png">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_app2.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="tab-fashion3" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/home_08.png">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div id="tab-fashion4" class="tab-content">
								<div class="owl-carousel">
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale">
												<p>SALE</p>
											</div>
											<img src="images/home_08.png">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p class="sale-price">$690.99  <span><strike>$720.00</strike></span></p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>

												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<div class="label-sale"><p>New</p> </div>
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_05.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
									<div class="item">
										<div class="h-best-slider-info">
											<img src="images/home_03.jpg">
											<div class="h-best-s-text">
												<h4>Product Name here</h4>
												<div class="pro-stars-rating"><div id="c-stars" class="starrr"></div></div>
												<div class="best-s-price">
													<p>$720.00 </p>
												</div>
												<div class="h-b-btn-cart">
													<a href="#">Add To Cart</a>
													<div class="h-b-btn-wish">
														<a href="#"><i class="zmdi zmdi-favorite"></i></a>
													</div>
												</div>
												<div class="h-b-qiuck-view">
													<a href="#">Quick view</a>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>

						</div>
					</div>
					</div><!--.home-app-wrapper--->
			</div><!--.row-->
		</div><!--.container-->
	</div><!--.fashion-wrapper-->
	<div class="clear"></div>
	<div class="recent-blog-wrapper">
			<div class="container">
			<div class="row">
			<div class="recents-blogs col-lg-12 col-xs-12">
				<div id="blogs_head" class="heading-wrap"><h2>Recent BLogs</h2></div><!--heading-wrap-->
				<div class="clear"></div>
				<div class="recent-blog-list">
					<div class="col-lg-3 col-xs-12 col-sm-6 recent-blog-cont">
						<div class="recent-blog-img"><a href="#_"><img src="images/index_63.jpg" alt="" /></a></div>
						<div class="r-title"><h6>It is a long established fact that a reader will be distracted</h6></div>
						<div class="r-meta"><label for="name">By: Gavin Yeung</label> &nbsp;&nbsp;&nbsp;&nbsp; April 16, 2017</div>
						<div class="r-post-text"><p>It is a long established fact that a reader will be distracted by the readable content of apage when looking at its layout</p></div>
					</div>
					
					<div class="col-lg-3 col-xs-12 col-sm-6 recent-blog-cont">
						<div class="recent-blog-img"><a href="#_"><img src="images/index_65.jpg" alt="" /></a></div>
						<div class="r-title"><h6>It is a long established fact that a reader will be distracted</h6></div>
						<div class="r-meta"><label for="name">By: Gavin Yeung</label> &nbsp;&nbsp;&nbsp;&nbsp; April 16, 2017</div>
						<div class="r-post-text"><p>It is a long established fact that a reader will be distracted by the readable content of apage when looking at its layout</p></div>
					</div>
					
					<div class="col-lg-3 col-xs-12 col-sm-6 recent-blog-cont">
						<div class="recent-blog-img"><a href="#_"><img src="images/index_67.jpg" alt="" /></a></div>
						<div class="r-title"><h6>It is a long established fact that a reader will be distracted</h6></div>
						<div class="r-meta"><label for="name">By: Gavin Yeung</label> &nbsp;&nbsp;&nbsp;&nbsp; April 16, 2017</div>
						<div class="r-post-text"><p>It is a long established fact that a reader will be distracted by the readable content of apage when looking at its layout</p></div>
					</div>
					
					<div class="col-lg-3 col-xs-12 col-sm-6 recent-blog-cont">
						<div class="recent-blog-img"><a href="#_"><img src="images/index_69.jpg" alt="" /></a></div>
						<div class="r-title"><h6>It is a long established fact that a reader will be distracted</h6></div>
						<div class="r-meta"><label for="name">By: Gavin Yeung</label> &nbsp;&nbsp;&nbsp;&nbsp; April 16, 2017</div>
						<div class="r-post-text"><p>It is a long established fact that a reader will be distracted by the readable content of apage when looking at its layout</p></div>
					</div>
					
					<div class="col-lg-3 col-xs-12 col-sm-6 recent-blog-cont">
						<div class="recent-blog-img"><a href="#_"><img src="images/index_67.jpg" alt="" /></a></div>
						<div class="r-title"><h6>It is a long established fact that a reader will be distracted</h6></div>
						<div class="r-meta"><label for="name">By: Gavin Yeung</label> &nbsp;&nbsp;&nbsp;&nbsp; April 16, 2017</div>
						<div class="r-post-text"><p>It is a long established fact that a reader will be distracted by the readable content of apage when looking at its layout</p></div>
					</div>
					
					<div class="col-lg-3 col-xs-12 col-sm-6 recent-blog-cont">
						<div class="recent-blog-img"><a href="#_"><img src="images/index_63.jpg" alt="" /></a></div>
						<div class="r-title"><h6>It is a long established fact that a reader will be distracted</h6></div>
						<div class="r-meta"><label for="name">By: Gavin Yeung</label> &nbsp;&nbsp;&nbsp;&nbsp; April 16, 2017</div>
						<div class="r-post-text"><p>It is a long established fact that a reader will be distracted by the readable content of apage when looking at its layout</p></div>
					</div>
				</div>
				</div>
			</div><!--.row-->
		</div><!--.container-->
	</div><!--.recent-blog-wrapper-->
	<div class="clear"></div>
	<div class="shop_category_container">
		<div class="container">
			<div class="row">
			<div id="blogs_head" class="heading-wrap"><h2>Shop by categories</h2></div><!--heading-wrap-->
				<div class="clear"></div>
				<div class="cat-listings col-xs-12 col-lg-12">
					<div class="cat-box col-lg-4 col-sm-6 noPadd">
					<section>
						<div class="col-md-6"><img src="images/index_80.jpg" alt=""></div>
						<div class="col-md-6 catlisting">
							<h6>Appliances</h6>
							<ul>
								<li><a href="#_">Accessories</a></li>
								<li><a href="#_">Car Electronics</a></li>
								<li><a href="#_">Desktops & Monitors</a></li>
								<li><a href="#_">Digital SLRs</a></li>
								<li><a href="#_">TV & Video</a></li>
							</ul>
						</div>
					</section>
					</div>
					<div class="cat-box col-lg-4 col-sm-6 noPadd">
					<section>
						<div class="col-md-6"><img src="images/index_83.jpg" alt=""></div>
						<div class="col-md-6 catlisting">
							<h6>Mobility</h6>
							<ul>
								<li><a href="#_">Accessories</a></li>
								<li><a href="#_">Car Electronics</a></li>
								<li><a href="#_">Desktops & Monitors</a></li>
								<li><a href="#_">Digital SLRs</a></li>
								<li><a href="#_">TV & Video</a></li>
							</ul>
						</div>
					</section>
					</div>
					<div class="cat-box col-lg-4 col-sm-6 noPadd">
					<section>
						<div class="col-md-6"><img src="images/index_85.jpg" alt=""></div>
						<div class="col-md-6 catlisting">
							<h6>Computing</h6>
							<ul>
								<li><a href="#_">Accessories</a></li>
								<li><a href="#_">Car Electronics</a></li>
								<li><a href="#_">Desktops & Monitors</a></li>
								<li><a href="#_">Digital SLRs</a></li>
								<li><a href="#_">TV & Video</a></li>
							</ul>
						</div>
					</section>
					</div>
					<div class="cat-box col-lg-4 col-sm-6 noPadd">
					<section>
						<div class="col-md-6"><img src="images/index_90.jpg" alt=""></div>
						<div class="col-md-6 catlisting">
							<h6>Wearables</h6>
							<ul>
								<li><a href="#_">Accessories</a></li>
								<li><a href="#_">Car Electronics</a></li>
								<li><a href="#_">Desktops & Monitors</a></li>
								<li><a href="#_">Digital SLRs</a></li>
								<li><a href="#_">TV & Video</a></li>
							</ul>
						</div>
					</section>
					</div>
					<div class="cat-box col-lg-4 col-sm-6 noPadd">
					<section>
						<div class="col-md-6"><img src="images/index_91.jpg" alt=""></div>
						<div class="col-md-6 catlisting">
							<h6>Television</h6>
							<ul>
								<li><a href="#_">Accessories</a></li>
								<li><a href="#_">Car Electronics</a></li>
								<li><a href="#_">Desktops & Monitors</a></li>
								<li><a href="#_">Digital SLRs</a></li>
								<li><a href="#_">TV & Video</a></li>
							</ul>
						</div>
					</section>
					</div>
					<div class="cat-box col-lg-4 col-sm-6 noPadd">
					<section>
						<div class="col-md-6"><img src="images/index_92.jpg" alt=""></div>
						<div class="col-md-6 catlisting">
							<h6>Accessories</h6>
							<ul>
								<li><a href="#_">Accessories</a></li>
								<li><a href="#_">Car Electronics</a></li>
								<li><a href="#_">Desktops & Monitors</a></li>
								<li><a href="#_">Digital SLRs</a></li>
								<li><a href="#_">TV & Video</a></li>
							</ul>
						</div>
					</section>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div><!--.primary-container-->
<?php
include('footer.php');
?>  
