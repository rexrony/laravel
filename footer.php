<footer class="footer">
 <div class="container">
 	<div class="row">
 		<div class="col-lg-12 footer-wrap">
 			<div class="footer-top">
 				<div class="footerboxes col-lg-3 col-sm-6">
 					<h5>CUSTOMER SERVICE</h5>
 					<section class="cus_srv">
 						<ul>
 							<li><label for="(800) 123 456 789">Phone:</label>(800) 123 456 789</li>
 							<li><label for="(800) 123 456 789">Fax:</label>(800) 123 456 789</li>
 							<li><label for="(800) 123 456 789">Email:</label><a href="mailto:contact@domian.com">contact@domian.com</a></li>
 						</ul>
 					</section>
 					<h5>Follow US</h5>
 					<section class="footer-social-icons">
 							<ul>
 							<li><a href="#_"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
 							<li><a href="#_"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
 							<li><a href="#_"><i class="fa fa-rss" aria-hidden="true"></i></a></li>
 							<li><a href="#_"><i class="fa fa-google" aria-hidden="true"></i></a></li>
 							<li><a href="#_"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						   </ul>
 					</section>
 				</div>
 				<div class="footerboxes col-lg-3 col-sm-6">
 					<h5>POPULAR SEARCHES</h5>
 					<div class="footer-menu">
 						<ul>
 							<li><a href="#_">IPhone 7</a></li>
 							<li><a href="#_">IPhone 6</a></li>
 							<li><a href="#_">Samsung Galaxy S7 Edge</a></li>
 							<li><a href="#_">International Shipping</a></li>
 							<li><a href="#_">OnePlus 3T</a></li>
 							<li><a href="#_">HTC U Ultra</a></li>
 							<li><a href="#_">Samsung TVs</a></li>
 							<li><a href="#_">Apple Macbooks</a></li>
 							<li><a href="#_">Abayas</a></li>
 							<li><a href="#_">Dresses</a></li>
 						</ul>
 					</div>
 				</div>
 				<div class="footerboxes col-lg-3 col-sm-6">
 					<h5>MY ACCOUNT</h5>
					<div class="footer-menu">
 						<ul>
 							<li><a href="#_">Log In / Register</a></li>
 							<li><a href="#_">My Shopping Cart</a></li>
 							<li><a href="#_">My Orders</a></li>
 							<li><a href="#_">My Addresses</a></li>
 							<li><a href="#_">Wish Lists</a></li>
 							<li><a href="#_">Account Settings</a></li>
 							<li><a href="#_">Account Summary</a></li>
 						</ul>
 					</div>
 					<h5>INTELLECTUAL PROPERTY</h5>
 					<div class="footer-menu">
 						<ul>
 							<li><a href="#_">Brand and Copyright Owners</a></li>
 							<li><a href="#_">Marketplace Sellers</a></li>
 						</ul>
 					</div>
 				</div>
 				<div class="footerboxes col-lg-3 col-sm-6">
 					<h5>SELLING ON Prime </h5>
 						<div id="forth-menu" class="footer-menu">
 						<ul>
 							<li><a href="#_">Sell on souq.com</a></li>
 							<li><a href="#_">How It Works</a></li>
 							<li><a href="#_">Selling Policies</a></li>
 							<li><a href="#_">Seller Terms and Conditions</a></li>
 							<li><a href="#_">Fulfilled by Souq</a></li>
 							<li><a href="#_">FAQs</a></li>
 						</ul>
 					</div>
 					<h5>BUYING ON Prime </h5>
 						<div class="footer-menu">
 						<ul>
 							<li><a href="#_">How to Buy</a></li>
 							<li><a href="#_">Easy Payment Plans</a></li>
 							<li><a href="#_">Return Policy</a></li>
 							<li><a href="#_">Advertise with us</a></li>
 						</ul>
 					</div>
 				</div>
 			</div>
 			<div class="clear"></div>
 			<div class="footer-bottom">
 				<section>
 					<div class="footer-menus">
 						<ul>
 							<li><a href="about.php">About Us </a></li>
 							<li><a href="#_">Media Center</a></li>
 							<li><a href="#_">Careers</a></li>
 							<li><a href="#_">Privacy Policy </a></li>
 							<li><a href="#_">Terms and Conditions</a></li>
 							<li><a href="#_">Affiliate Program</a></li>
 							<li><a href="#_">Helpbit Services</a></li>
 						</ul>
 					</div>
 					<div class="clear"></div>
 					<div class="copyright">
 						<p>Copyright &copy; <?php echo date('Y'); ?> by <span>LARAVEL</span> rights reserved</p>
 					</div>
 				</section>
 			</div>
 		</div>
 	</div><!--.row-->
 </div><!--.container-->
</footer><!--.footer-->
<div class="sticky-sidebar hidden-xs hidden-sm">
	<ul id="scroll-item">
			<li><a href="#cat_1" class="cat_1 _mPS2id-h"><strong>text</strong></a></li>
			<li><a href="#cat_2" class="cat_2 _mPS2id-h"><strong>text</strong></a></li>
			<li><a href="#cat_3" class="cat_3 _mPS2id-h"><strong>text</strong></a></li>
			<li><a href="#cat_4" class="cat_4 _mPS2id-h"><strong>text</strong></a></li>
		</ul>
</div>


<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="js/owl.carousel.js"></script>
<script src="js/slick.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery.slicknav.js"></script>
<script src="js/wow.js"></script>
<script src="js/script.js"></script>
<script type="text/javascript">

</script>


</body>
</html>