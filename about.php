<?php
include('header.php');
?>
<div class="primary-container inner-content">
<div class="container">
	<div class="row">
		<div class="col-lg-12 main-wrapper">
			<div class="col-xs-12 col-lg-4 noPadd-lft left-sidebar-wrapper">
				<section class="left-sidebar">
					<div class="cus-support-list">
						<ul>
							<li class="hvr-shrink wow fadeInLeft" data-wow-delay="1s">
								<div class="cus-icon"></div>
								<div class="cus-supp-heading">
								<h6>Shipping & Returns</h6>
								<span>World wide shipping</span>
								</div>
							</li>
							<li class="hvr-shrink wow fadeInLeft" data-wow-delay="1.2s">
								<div class="cus-icon"></div>
								<div class="cus-supp-heading">
								<h6>Money Back</h6>
								<span>Guaranted payments</span>
								</div>
							</li>
							<li class="hvr-shrink wow fadeInLeft" data-wow-delay="1.3s">
								<div class="cus-icon"></div>
								<div class="cus-supp-heading">
								<h6>Support Policy</h6>
								<span>Fast support team</span>
								</div>
							</li>
							<li class="hvr-shrink wow fadeInLeft" data-wow-delay="1.4s">
								<div class="cus-icon"></div>
								<div class="cus-supp-heading">
								<h6>Shipping & Returns</h6>
								<span>World wide shipping</span>
								</div>
							</li>
						</ul>
					</div>
					<div class="clear"></div>
					<div class="follow-us-wrap">
					<h5>Follow us</h5>
						<ul>
							<li class="hvr-pulse"><a href="#_"><img src="images/fb-icon.jpg" alt=""></a></li>
							<li class="hvr-pulse"><a href="#_"><img src="images/tweet-icon.jpg" alt=""></a></li>
							<li class="hvr-pulse"><a href="#_"><img src="images/google-plus.jpg" alt=""></a></li>
							<li class="hvr-pulse"><a href="#_"><img src="images/linkedin-ico.jpg" alt=""></a></li>
							<li class="hvr-pulse"><a href="#_"><img src="images/pinrest-ico.jpg" alt=""></a></li>
							<li class="hvr-pulse"><a href="#_"><img src="images/emial-ico.jpg" alt=""></a></li>
						</ul>
					</div>
					<img class="img-responsive" src="images/About_40.jpg" alt="">
				</section>
			</div>
			<div class="col-lg-8 wow fadeInRight" data-wow-delay="1.2s">
				<div class="abt-wrap">
					<figure><img class="img-responsive" src="images/About_03.jpg" alt=""></figure>
					<h4>About <span>LARAVEL</span></h4>
					<section class="abt-text-con">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mollis mattis lectus, sed pharetra arcu efficitur quis. Maecenas semper neque quis magna dignissim, sed ultricies risus gravida. Duis viverra nibh sit amet enim rutrum finibus eget in metus. Fusce dui erat, posuere sit amet turpis non, tempor iaculis eros. Donec malesuada turpis ac enim viverra, vel posuere dolor tempus. Aliquam nec enim sed eros tempus porta. Proin metus eros, mollis ut luctus vel, blandit nec mi. Nulla egestas, felis eu varius mattis, eros augue cursus dui, ac congue lectus n</p>

<p>ulla eu velit.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mollis mattis lectus, sed pharetra arcu efficitur quis. Maecenas semper neque quis magna dignissim, sed ultricies risus gravida. Duis viverra nibh sit amet enim rut</p>
					</section>
				</div>
				<div class="clear"></div>
				<div class="abt-tabbing" id="tabs-container">
						<ul class="tabs-menu">
							<li class="current"><a href="#tab-1-abt" class="hh-click1">History & Mission</a></li>
							<li><a href="#tab-2-abt">Tab 2 here</a></li>
							<li><a href="#tab-3-abt">Tab 3 here</a></li>
							</ul>
						<div class="tab">
							<div id="tab-1-abt" class="tab-content">
								<section class="abt-tabs">
									<div class="histroy-mission">
										<h3>LARAVEL History and Mission</h3>
										<p><i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mollis mattis lectus, sed pharetra arcu efficitur quis. Maecenas semper neque quis magna dignissim, sed ultricies risus gravida. Duis viverra nibh sit amet enim rutrum finibus eget in metus. Fusce dui erat, posuere sit amet turpis non, tempor iaculis eros. Donec malesuada turpis ac enim viverra, vel posuere dolor tempus. </i></p>
									</div>
								</section>
							</div>
							<div id="tab-2-abt" class="tab-content">
								<section class="abt-tabs">
									<div class="histroy-mission">
										<h3>LARAVEL History and Mission</h3>
										<p><i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mollis mattis lectus, sed pharetra arcu efficitur quis. Maecenas semper neque quis magna dignissim, sed ultricies risus gravida. Duis viverra nibh sit amet enim rutrum finibus eget in metus. Fusce dui erat, posuere sit amet turpis non, tempor iaculis eros. Donec malesuada turpis ac enim viverra, vel posuere dolor tempus. </i></p>
									</div>
								</section>
							</div>
							<div id="tab-3-abt" class="tab-content">
								<section class="abt-tabs">
									<div class="histroy-mission">
										<h3>LARAVEL History and Mission</h3>
										<p><i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mollis mattis lectus, sed pharetra arcu efficitur quis. Maecenas semper neque quis magna dignissim, sed ultricies risus gravida. Duis viverra nibh sit amet enim rutrum finibus eget in metus. Fusce dui erat, posuere sit amet turpis non, tempor iaculis eros. Donec malesuada turpis ac enim viverra, vel posuere dolor tempus. </i></p>
									</div>
								</section>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<?php
include('footer.php');
?>  
