<!DOCTYPE html>
<html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1"><title>Laravel- Home Page</title>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700|Raleway:300,400,500,600,800|Poppins:300,400,500,600" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<!--.slick slide-->
<link rel="stylesheet" href="css/slick-theme.css">
<link rel="stylesheet" type="text/css" href="css/slick.css">
<!--.slick Nav-->
<link rel="stylesheet" type="text/css" href="css/slicknav.min.css">
<!--.Animate Css-->
<link rel="stylesheet" type="text/css" href="css/animate.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<link rel="stylesheet" href="css/hover.css">
</head><body>
<header id="vg-header-wrapper"><div class="clear-float top-bar"><div class="container"><div class="row"><div class="col-xs-6 col-topbar col-1"><div class="inside"><div id="text-4" class="widget widget_text"><div class="textwidget"><ul class="top-static"><li>
<span class="st-label">Email: </span>
<span class="st-text">contact@domain.com</span></li><li>
<span class="st-label">Phone: </span>
<span class="st-text">0123-456-789</span></li></ul></div></div></div></div>
<!-- End Top Bar 01 Widget --><div class="col-xs-6 col-topbar col-2"><div class="inside"><div id="nav_menu-2" class="widget widget_nav_menu"><div class="menu-account-container"><ul id="menu-account" class="menu"><li><a href="#_">My Account</a><ul class="sub-menu"><li><a href="#_">Cart</a></li><li><a href="#_">Checkout</a></li><li><a href="#_">Wishlist</a></li><li><a href="#_">Order Tracking</a></li></ul></li></ul></div></div><div id="text-2" class="widget widget_text"><div class="textwidget"><div class="lang-cur currency">
<span class="title">Currency:</span><div class="select">
<span class="selected">USD</span><ul><li>Dollar (USD)</li><li>Euro (EUR)</li></ul></div></div></div></div><div id="text-3" class="widget widget_text"><div class="textwidget"><div class="lang-cur languages">
<span class="title">Languages:</span><div class="select">
<span class="selected">English</span><ul><li>English</li><li>Japanese</li></ul></div></div></div></div></div></div>
<!-- End Top Bar 01 Widget --></div></div></div>
<!-- End top-bar --><div class="header"><div class="container"><div class="row"><div id="logo-wrapper" class="col-xs-12 col-lg-3"><div class="logo-inside">
<a href="/" rel="home"><span class="logo-background wow flipInX" data-wow-delay="1s">Laravel</span></a></div>
</div>
<!-- End site-logo -->
<div class="col-xs-12 col-lg-9 top-feature">
	<div class="top-cart">
		<div class="cart-inside">
			<div class="widget_shopping_cart">
				<div class="widget_shopping_cart_content">
					<div class="mini_cart_content">
						<div class="mini_cart_inner">
							<div class="top-cart-title">
								<a href="#_">
									<div class="shopping_cart"><span class="icon-cart"><i class="zmdi zmdi-shopping-cart"></i>
										<span class="cart-quantity">0</span>
										</span>
										<span class="sub-title">My Cart </span>
										<span class="cart-total-price"><span class="title">Price: </span><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">$</span>0.00</span>
										</span>
									</div>
								</a>
							</div>
							<div class="mcart-border" style="visibility: hidden;">
								<ul class="cart_empty ">
									<li>You have no items in your shopping cart</li>
									<li class="total">Subtotal: <span class="amount"><span class="price-currencySymbol">$</span>0.00</span>
									</li>
								</ul>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="top-search">
		<span class="search-toggle"><i class="zmdi zmdi-search"></i></span>
		<div class="search-inside">
			<div class="search-popup-bg"></div>
			<div id="vg_alice_product_search-2" class="widget widget_vg_alice_product_search">
				<div class="vina-product-search ">
					<form role="search" method="get" action="">
					<label class="screen-reader-text" for="s">Search for:</label>
<div class="select-category">
				<select name="category" class="SlectBox" onclick="" onchange="console.log('change is firing')">
					<option value="0" selected="selected">Select Category</option><option value="accessories">Accessories</option><option value="bags-belts">--Bags &amp; Belts</option><option value="scarves-gloves">--Scarves &amp; Gloves</option><option value="socks-tights">--Socks &amp; Tights</option><option value="sunglasses-readers">--Sunglasses &amp; Readers</option><option value="book-office">Book &amp; Office</option><option value="computer-laptop">Computer -Laptop</option><option value="digital">Digital</option><option value="electronics">Electronics</option><option value="all-in-one-computers">--All-in-One Computers</option><option value="gaming-desktops">--Gaming Desktops</option><option value="refurbished-desktops">--Refurbished Desktops</option><option value="towers-only">--Towers Only</option><option value="fashion">Fashion</option><option value="clothing">--Clothing</option><option value="coats-jackets">----Coats Jackets</option><option value="jackets">----Jackets</option><option value="raincoats">----Raincoats</option><option value="t-shirts">----T-shirts</option><option value="dresses">--Dresses</option><option value="day">----Day</option><option value="evening">----Evening</option><option value="sports">----Sports</option><option value="sweater">----Sweater</option><option value="handbags">--Handbags</option><option value="coats">----Coats</option><option value="kids-handbags">----Kids</option><option value="satchels">----Satchels</option><option value="shoulder">----Shoulder</option><option value="shoes">--Shoes</option><option value="ankle-boots">----Ankle Boots</option><option value="books">----Books</option><option value="clog-sandals">----Clog sandals</option><option value="run">----Run</option><option value="furniture-decor">Furniture &amp; Decor</option><option value="bedroom">--Bedroom</option><option value="dining-room">--Dining room</option><option value="home-office">--Home &amp; Office</option><option value="living-room">--Living room</option><option value="health-beaty">Health &amp; Beaty</option><option value="jewely-watch">Jewely &amp; Watch</option><option value="kids">Kids</option><option value="smartphone-tablets">Smartphone &amp; Tablets</option><option value="apple-phones">--Apple Phones</option><option value="lenovo-phones">--Lenovo Phones</option><option value="motorola-phones">--Motorola Phones</option><option value="samsung-phones">--Samsung Phones</option><option value="sport-outdoor">Sport &amp; Outdoor</option></select></div>
						<input name="s" id="s" type="text" placeholder="Search for products" value="" /><input type="hidden" name="post_type" value="product" /><button type="submit">Search</button></form>
				</div>
			</div>
		</div>
	</div>
<!-- End Widget Top Search --></div>
<!-- End top-feature --></div></div></div>
<!-- End Header Container -->
<div class="menu-wrapper">
<div class="container">
<div class="inner-container">
<div class="row">
<div class="col-xs-12 col-md-3 top-category">
<div class="inside">
<h3 class="btn-categories">All categories</h3>
<div class="category-inside white">
<div class="nano has-scrollbar">
<div id="vg_alice_category_treeview-3" class="widget widget_vg_alice_category_treeview">
<div id="vgw-category-treeview-2" class="vg-alice-category-treeview ">
<ul class="level0 filetree treeview">
<li class=" hvr-shrink"><a href="#"><span class="catTitle">Computer</span></a>
<ul class="sub-menu" style="display:none;">
<li class=" hvr-shrink"><a href="#_" title="Bags &amp; Belts"><span class="catTitle">Bags &amp; Belts</span></a></li>
<li class=" hvr-shrink"><a href="#_" title="Scarves &amp; Gloves"><span class="catTitle">Scarves &amp; Gloves</span></a></li>
<li class=" hvr-shrink"><a href="#_" title="Socks &amp; Tights"><span class="catTitle">Socks &amp; Tights</span></a></li>
<li class=" hvr-shrink"><a href="#_"><span class="catTitle hvr-shrink">Sunglasses &amp; Readers</span></a>
</li>
</ul>
<li class=" hvr-shrink"><a href="#_"><span class="catTitle">Electronics</span></a></li>
<li class=" hvr-shrink"><a href="#_"><span class="catTitle">Flowers & Gifts</span></a></li>
<li class=" hvr-shrink"><a href="#_"><span class="catTitle">Furniture</span></a></li>
<li class=" hvr-shrink"><a href="#_"><span class="catTitle">Groceries</span></a></li>
<li class=" hvr-shrink"><a href="#_"><span class="catTitle">Health & Beauty</span></a></li>
<li class=" hvr-shrink"><a href="#_"><span class="catTitle">Home Improvement</span></a></li>
<li class=" hvr-shrink"><a href="#_"><span class="catTitle">Clothing</span></a></li>
<li class=" hvr-shrink"><a href="#_"><span class="catTitle">Books</span></a></li>
<li class=" hvr-shrink"><a href="#_"><span class="catTitle">Jewelery & Watches</span></a></li>

</ul>
</div>
</div>
	
</div>
</div>
</div>
</div>
<div id="navigation" class="col-xs-12 col-md-9">
<div class="site-navigation visible-lg">
<nav class="main-navigation default-navigation white">
<ul class="menu-main-menu">
<li><a href="#_">Prime</a></li>
<li><a href="#_">Appliances</a></li>
<li><a href="#_">Mobility</a></li>
<li><a href="#_">Computing</a></li>
<li><a href="#_">Wearables</a></li>
<li><a href="#_">Television</a></li>
<li><a href="#_">Accessories</a></li>
<li><a href="#_">Smart power Solutions </a></li>
</ul>
</nav>
<!-- .main-navigation --></div>
<!-- End site-navigation --><div class="responsive-navigation visible-xs"><ul><li class="offcanvas-menu-button">
<a class="tools_button">
<span class="menu-button-text">Menu</span>
<span class="tools_button_icon">
<i class="fa fa-bars"></i>
</span>
</a></li></ul></div>
<!-- End mobile-navigation --></div>
<!-- End #navigation --></div></div></div></div>
</header>