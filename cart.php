<?php
include('header.php');
?>
<div id="cart-page" class="primary-container inner-content">
<div class="container">
	<div class="row">
		<div class="col-lg-12 main-wrapper">
		<div class="row">
	  <div class="col-md-8 cart-wrapper">
	  	<div class="cart-heading">
	  		<h2>Shopping CART <span>(2)</span></h2>
	  	</div>
	  <div class="cart-boxes">
	  <div class="rows">
  		<section class="cart-top-section">
			<div class="cart-prod-img col-lg-3">
  			<figure class="cart-padd thumbnail"><img class="img-responsive" src="images/Cart_03.jpg" alt=""></figure>
	  	    </div><!--.cart-prod-img-->
  		<div class="col-md-9">
  			<div class="pro-cart-heading"><p>Canon EOS 1200D 18-55mm III Lens Kit + Canon EF 75-300mm f/4..</p></div>
  			<div class="row">
  			<div class="col-md-4 col-sm-6">
  				<div class="pro-cart-price">$1,593.00</div>
  				<div class="clear"></div>
  				<div class="pro-cart-meta">
  					<ul>
  						<li><label for="">Sold by:</label> <span>low price uae</span></li>
  						<li><label for="">Condition:</label> <span>New</span></li>
  						<li><label for="">Color:</label> <span>Black</span></li>
  					</ul>
  				</div>
  			</div>
  			<div class="col-md-6 col-sm-6 pull-right">
  				<div class="qty-box-wrap pull-right">
  					<label class="qty-label" for="qty-label">Qty:</label>
 				    <div class="quantity  buttons_added">	
 				    <form id='cart-qty-form' method='POST' action='#'>
  				    <input type="button" value="+" class="plus" field='quantity' />
  					<input type="number" class="qty_box" size="4" value="0" maxlength="4" name='quantity' />
  					<input type="button" value="-" class="minus" field='quantity' />
					</form>
					</div>
  				</div>
  				<br class="clear" />
  				<div class="qty-stock">Order now, only 1 left in stock!</div>
  			</div>
  			</div>
  		</div>
  		</section>
 	  	<section class="cart-btn-section">
 	  	<div class="col-md-4 noPadd">
 	  		<ul>
 	  			<li><a href="#_">Save For later</a></li>
 	  			<li><a href="#_">Delete</a></li>
 	  		</ul>
 	  	</div>
 	  		
 	  	</section><!--.cart-btn-section-->
  	  		
	  </div>
	  </div><!--.cart-boxes-->
	  <div class="cart-boxes">
	  <div class="rows">
  		<section class="cart-top-section">
			<div class="cart-prod-img col-lg-3">
  			<figure class="cart-padd thumbnail"><img class="img-responsive" src="images/Cart_03.jpg" alt=""></figure>
	  	    </div><!--.cart-prod-img-->
  		<div class="col-md-9">
  			<div class="pro-cart-heading"><p>Canon EOS 1200D 18-55mm III Lens Kit + Canon EF 75-300mm f/4..</p></div>
  			<div class="row">
  			<div class="col-md-4 col-sm-6">
  				<div class="pro-cart-price">$1,593.00</div>
  				<div class="clear"></div>
  				<div class="pro-cart-meta">
  					<ul>
  						<li><label for="">Sold by:</label> <span>low price uae</span></li>
  						<li><label for="">Condition:</label> <span>New</span></li>
  						<li><label for="">Color:</label> <span>Black</span></li>
  					</ul>
  				</div>
  			</div>
  			<div class="col-md-6 col-sm-6 pull-right">
  				<div class="qty-box-wrap pull-right">
  					<label class="qty-label" for="qty-label">Qty:</label>
 				    <div class="quantity  buttons_added">	
 				    <form id='cart-qty-form' method='POST' action='#'>
  				    <input type="button" value="+" class="plus" field='quantity' />
  					<input type="number" class="qty_box" size="4" value="0" maxlength="4" name='quantity' />
  					<input type="button" value="-" class="minus" field='quantity' />
					</form>
					</div>
  				</div>
  				<br class="clear" />
  				<div class="qty-stock">Order now, only 1 left in stock!</div>
  			</div>
  			</div>
  		</div>
  		</section>
 	  	<section class="cart-btn-section">
 	  	<div class="col-md-4 noPadd">
 	  		<ul>
 	  			<li><a href="#_">Save For later</a></li>
 	  			<li><a href="#_">Delete</a></li>
 	  		</ul>
 	  	</div>
 	  		
 	  	</section><!--.cart-btn-section-->
  	  		
	  </div>
	  </div><!--.cart-boxes-->
	  <div class="cart-boxes">
	  <div class="rows">
  		<section class="cart-top-section">
			<div class="cart-prod-img col-lg-3">
  			<figure class="cart-padd thumbnail"><img class="img-responsive" src="images/Cart_03.jpg" alt=""></figure>
	  	    </div><!--.cart-prod-img-->
  		<div class="col-md-9">
  			<div class="pro-cart-heading"><p>Canon EOS 1200D 18-55mm III Lens Kit + Canon EF 75-300mm f/4..</p></div>
  			<div class="row">
  			<div class="col-md-4 col-sm-6">
  				<div class="pro-cart-price">$1,593.00</div>
  				<div class="clear"></div>
  				<div class="pro-cart-meta">
  					<ul>
  						<li><label for="">Sold by:</label> <span>low price uae</span></li>
  						<li><label for="">Condition:</label> <span>New</span></li>
  						<li><label for="">Color:</label> <span>Black</span></li>
  					</ul>
  				</div>
  			</div>
  			<div class="col-md-6 col-sm-6 pull-right">
  				<div class="qty-box-wrap pull-right">
  					<label class="qty-label" for="qty-label">Qty:</label>
 				    <div class="quantity  buttons_added">	
 				    <form id='cart-qty-form' method='POST' action='#'>
  				    <input type="button" value="+" class="plus" field='quantity' />
  					<input type="number" class="qty_box" size="4" value="0" maxlength="4" name='quantity' />
  					<input type="button" value="-" class="minus" field='quantity' />
					</form>
					</div>
  				</div>
  				<br class="clear" />
  				<div class="qty-stock">Order now, only 1 left in stock!</div>
  			</div>
  			</div>
  		</div>
  		</section>
 	  	<section class="cart-btn-section">
 	  	<div class="col-md-4 noPadd">
 	  		<ul>
 	  			<li><a href="#_">Save For later</a></li>
 	  			<li><a href="#_">Delete</a></li>
 	  		</ul>
 	  	</div>
 	  		
 	  	</section><!--.cart-btn-section-->
  	  		
	  </div>
	  </div><!--.cart-boxes-->
	  </div><!--.cart-wrapper--->
	  <div class="cart-right-sidebar col-md-3">
			<div class="cart-total-con">
				<label for="">Total:</label> <span class="total-price"><strong>$3,642.00</strong></span>
			</div>
			<section class="cart-meta">
			<h5>Extra Discounts</h5>
			<ul>
				<li>Pay 3,592.00 AED ( Save 50.00 AED ) By using ADCB Card</li>
				<li>Pay 3,592.00 AED ( Save 50.00 AED ) By using ADCB Card</li>
			</ul>
			<span>Part of your order qualifies for FREE Shipping</span>
		  </section>
		    <section class="cart-btns">
		    	<a href="#_" class="hvr-shrink checkout-btn">Proceed to Checkout</a>
		    	<br>
		    	<a href="#_" class="hvr-shrink add-coupon-btn">Add a coupon</a>
		    </section>
			<section class="cart-side">
				<img class="img-responsive" src="images/Cart_21.jpg" alt="">
				<br>
				<img class="img-responsive" src="images/About_40.jpg" alt="">
			</section>
	 </div>
		</div><!--.row-->
		
 </div><!--.main-wrapper-->
 </div><!--.row-->
 </div><!--.container-->
</div><!--#cart-page-->
</div>

<?php
include('footer.php');
?>  
